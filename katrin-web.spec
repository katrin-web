Name: katrin-web
Version: 1.1.0
Release: alt1

%define installdir /var/www/html/addon-modules/%name

Summary: Web interface for Katrin billing system
License: GPL
Group: Networking/WWW
Requires: python-module-flup python-module-django-dbbackend-mysql

URL: http://katrin.sf.net
Packager: Denis Klimov <zver@altlinux.ru>
BuildArch: noarch

Source: katrin-web.tar
Source1: katrin-web.conf

%description
Web interface for Katrin billing system.

%package apache
Summary: Apache conf files for katrin-web
Group: Networking/WWW
Requires: %name = %version-%release
Requires: apache
Requires: mod_fastcgi
Requires: python-module-django >= 0.97

%description apache
This package provides Apache config files for katrin-web.

%prep
%setup -n katrin-web

%build

%install
install -pD %SOURCE1 %buildroot/etc/httpd/conf/addon-modules.d/katrin-web.conf
install -pD -m 644 %name.sql %buildroot%_datadir/%name/%name.sql

mkdir -p %buildroot%installdir
cp -pR katrin-web/* %buildroot%installdir

%post apache
%post_service httpd

%postun apache
%post_service httpd

%files
%installdir/
%_datadir/%name/
%config(noreplace) %installdir/settings.py

%files apache
%config(noreplace) %_sysconfdir/httpd/conf/addon-modules.d/katrin-web.conf


%changelog
* Sun May 04 2008 Denis Klimov <zver@altlinux.ru> 1.1.0-alt1
- add statistics by filters feature

* Tue Apr 29 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt2
- add store stat field in models

* Tue Mar 11 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt1.RC0
- make separate django applications
- support 1.0.0-RC1 katrin database schema

* Tue Dec 25 2007 Denis Klimov <zver@altlinux.ru> 0.4-alt1
- modify models
- add menu in stat pages
- add define encoding

* Thu Dec 13 2007 Denis Klimov <zver@altlinux.ru> 0.3-alt1
- show traffic bytes as float
- add cost in stats of month and order by it
- add RequestContext in return functions for show userlink in top of admin panel

* Wed Dec 05 2007 Denis Klimov <zver@altlinux.ru> 0.2-alt1
- build for 0.2 version

* Thu Nov 29 2007 Denis Klimov <zver@altlinux.ru> 0.1-alt1
- init build for ALT Linux


