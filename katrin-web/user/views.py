from django.shortcuts import render_to_response
from katrin.models import Users

def user_wellcome(request):
	if (request.method == 'POST' ) :
		return user_show_stats(request)
	else:
		return render_to_response('user_login.html')

def user_show_stats(request):
	user = Users.objects.filter(login=request.POST['login'], password=request.POST['password'])
	if user:
		user = Users.objects.get(login=request.POST['login'], password=request.POST['password'])
		return render_to_response('user_stats.html',
			{'user':user})
	else:
		return user_access_denied(request)

def user_access_denied(request):
	return render_to_response('user_access_denied.html')

