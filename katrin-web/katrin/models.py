# vim: set fileencoding=utf-8 :

from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Tariffs(models.Model):
	id = models.IntegerField(primary_key=True)
	title = models.CharField("Название", max_length=120)
	perday = models.DecimalField("За день",max_digits=5, decimal_places=2, help_text='Абоненская плата за день')
	permonth = models.DecimalField("За месяц", max_digits=5, decimal_places=2, help_text='Абоненская плата за месяц')
	week_days = models.CharField("Дни недели", max_length=15, blank=True, help_text="Номера дней недели через запятую (например: 6,7). Тариф будет активен в эти дни.")
	class Meta:
		db_table = u'tariffs'
		verbose_name = 'тариф'
		verbose_name_plural = 'Тарифы'
	def __unicode__(self):
		return "%s" % (self.title)
	class Admin:
		fields = (
		('Информация о тарифе', {'fields': ('title','perday','permonth','week_days')}),
		)
		list_display = ("title", "perday", "permonth", "week_days")
		ordering = ["title"]
		search_fields = ("title")

YES_NO = (
	(1, 'Да'),
	(0, 'Нет'),
)

class Users(models.Model):
	id = models.IntegerField(primary_key=True)
	login = models.CharField("Логин", max_length=60)
	password = models.CharField("Пароль", max_length=20)
	lastname = models.CharField("Фамилия", max_length=20)
	firstname = models.CharField("Имя", max_length=20)
	middlename = models.CharField("Отчество", max_length=20)
	tariffid = models.ForeignKey(Tariffs,db_column='tariffid')
	balance = models.DecimalField(max_digits=7, decimal_places=4)
	credit = models.DecimalField(max_digits=7, decimal_places=4)
	blocked = models.IntegerField(default=0, choices=YES_NO)
	class Meta:
		db_table = u'users'
		verbose_name = 'клиента'
		verbose_name_plural = 'Клиенты'
	def __unicode__(self):
			return "%s" % (self.login)
	class Admin:
		fields = (
			('Основная информация', {'fields': ('login','password','lastname','firstname','middlename',)}),
			('Счет абонента', {'fields': ('balance', 'credit', 'tariffid', 'blocked')}),
		)
		list_display = ("login", "balance", "credit", "tariffid")
		ordering = ["login"]
		search_fields = ("login", "lastname")
		list_filter = ("tariffid",)

class RefillLog(models.Model):
	id = models.AutoField(primary_key=True)
	datetime = models.DateTimeField(default=datetime.now())
	adminid = models.ForeignKey(User,db_column='adminid')
	userid = models.ForeignKey(Users,db_column='userid')
	refill_size = models.DecimalField("Пополнено на",max_digits=5, decimal_places=2, help_text='Размер пополнения')
	class Meta:
		verbose_name = 'пополнение счета'
		verbose_name_plural = 'Логи пополнения счетов'
	class Admin:
	    list_display = ("datetime", "adminid","userid","refill_size")


# This classes only for show links in admin main page.
class Refill(models.Model):
	id = models.AutoField(primary_key=True)
	class Meta:
		db_table = u'not_table'
		verbose_name_plural = 'Пополнить баланс'
	class Admin:
		pass
