# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from katrin.models import Users, RefillLog
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

def refill(request):
    if request.user.is_authenticated():
	if (request.method == 'POST'):
		refill_size = float(request.POST['refill_size'])
		user = Users.objects.get(pk=request.POST['userid'])
		user.balance = user.balance + refill_size
		user.save()
		operator=request.user.id
		refill_log = RefillLog(
						adminid = User.objects.get(pk=request.user.id),
						userid = user,
						refill_size = refill_size)
		refill_log.save()

		return render_to_response("refill_done.html", 
						{
						'user':user,
						}, 
						context_instance=RequestContext(request))
	else:
		users = Users.objects.all()
		return render_to_response("refill.html", 
						{
						'users':users,
						},
						context_instance=RequestContext(request))
    else: 
	    return HttpResponseRedirect("../../../admin/")