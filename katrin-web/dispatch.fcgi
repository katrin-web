#!/usr/bin/python

import sys
sys.path.append('/var/www/html/addon-modules')
  
from flup.server.fcgi import WSGIServer
from django.core.handlers.wsgi import WSGIHandler

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'katrin-web.settings'
WSGIServer(WSGIHandler()).run()

