from django.conf.urls.defaults import *


urlpatterns = patterns('',
	(r'^$', include('katrin-web.user.urls')),
	(r'^user/', include('katrin-web.user.urls')),
	(r'^admin/katrin/', include('katrin-web.katrin.urls')),
	(r'^admin/traff/servicetraffstats/', include('katrin-web.traff.urls')),
	(r'^admin/tel/servicetelstatsaggregate/', include('katrin-web.tel.urls')),
	(r'^admin/', include('django.contrib.admin.urls')),
)

