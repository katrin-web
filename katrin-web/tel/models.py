# vim: set fileencoding=utf-8 :

from django.db import models
from katrin.models import *

class ServiceTelFilters(models.Model):
	id = models.IntegerField(primary_key=True)
	title = models.CharField(max_length=40)
	tariffid = models.ForeignKey(Tariffs,db_column='tariffid')
	priority = models.IntegerField("Приоритет")
	store_stat = models.IntegerField()
	src_number = models.CharField(max_length=60, null=True, blank=True)
	dst_number = models.CharField(max_length=60, null=True, blank=True)
	src_trunk = models.CharField(max_length=45, null=True, blank=True)
	dst_trunk = models.CharField(max_length=45, null=True, blank=True)
	in_call = models.IntegerField(default=0)
	persecond_in = models.DecimalField(max_digits=5, decimal_places=2)
	persecond_out = models.DecimalField(max_digits=5, decimal_places=2)
	class Meta:
		db_table = u'service_tel_filters'
		verbose_name = 'фильтр телефонии'
		verbose_name_plural = 'Фильтры телефонии'
	def __unicode__(self):
			return "%s-%s" % (self.tariffid, self.priority)
	class Admin:
		fields = (
			('Основные параметры', {'fields': ('title', 'tariffid', 'priority', 'store_stat')}),
			('Специфические параметры', {'fields': ('src_number', 'dst_number', 'src_trunk', 'dst_trunk', 'in_call', 'persecond_in', 'persecond_out')}),
		)
		list_display = ("title", "tariffid", "priority", "store_stat", "src_number", "dst_number", 'src_trunk', 'dst_trunk')
		ordering = ["tariffid"]
		search_fields = ("tariffid")
		list_filter = ("tariffid",)

IN_CALL = ( 
	(1, 'Входящий'),
	(2, 'Исходящий'),
	(0, 'None'), 
)


class ServiceTelStats(models.Model):
	id = models.IntegerField(primary_key=True)
	userid = models.ForeignKey(Users,db_column='userid', verbose_name='Клиент')
	storetime = models.DateTimeField('Дата')
	cost = models.DecimalField('Стоимость', max_digits=5, decimal_places=2)
	pbx = models.CharField('АТС', max_length=20)
	src_number = models.CharField('SRC номер', max_length=60)
	dst_number = models.CharField('DST номер', max_length=60)
	src_trunk = models.CharField('SRC поток', max_length=45)
	dst_trunk = models.CharField('DST поток', max_length=45)
	in_call = models.IntegerField('Направление', choices=IN_CALL)
	duration = models.IntegerField('Продолжительность')
	class Meta:
		db_table = u'service_tel_stats'
		verbose_name = 'статистика телефонии'
		verbose_name_plural = 'Статистика телефонии'
	class Admin:
		list_display = ("userid", "storetime", "cost", "pbx", "src_number", "dst_number", "src_trunk", "dst_trunk", "in_call", "duration")
		list_filter = ("storetime",)
		ordering = ["-storetime"]
		search_fields = ("storetime", "userid")

class ServiceTelUparams(models.Model):
	id = models.IntegerField(primary_key=True)
	userid = models.ForeignKey(Users,db_column='userid', verbose_name='Клиент')
	access_type = models.CharField('Тип доступа', max_length=45, null=True, blank=True)
	tel_number = models.CharField('Телефонный номер', max_length=60)
	class Meta:
		db_table = u'service_tel_uparams'
		verbose_name = 'параметры телефонии'
		verbose_name_plural = 'Параметры телефонии'
	class Admin:
		list_display = ("userid", "access_type", "tel_number")
		ordering = ["userid"]
		search_fields = ("tel_number", "userid")

#	Model for show link in main page of admin panel
class ServiceTelStatsAggregate(models.Model):
	id = models.IntegerField(primary_key=True)
	class Meta:
		db_table = u'not_table'
		verbose_name_plural = 'Статистика телефонии (аггрегированная)'
	class Admin:
		pass

