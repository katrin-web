# vim: set fileencoding=utf-8 :

# TODO: Replace all aggregate query to DB API when it'll be work in Django.
# See: http://code.djangoproject.com/ticket/3566

from django.shortcuts import get_object_or_404, render_to_response
#from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
import datetime, time

from katrin.models import Users
from tel.models import ServiceTelStats
from tel.models import ServiceTelUparams

from django.db import connection

from django import newforms as forms
from django.newforms import extras

from django.utils import encoding

from django.utils.safestring import mark_safe

IN_CALL = ( 
	('-1', 'Все'),
	('1', 'Входящие'),
	('2', 'Исходящие'),
	('0', 'Другие'), 
)

GROUP_BY = (
	('none','---'),
	('users','Пользователям'),
	('days','Дням'),
	('src_number','SRC номерам'),
	('dst_number','DST номерам'),
	('src_trunk','SRC потокам'),
	('dst_trunk','DST потокам'),
	('in_call','направлениям'),
)

# for get uniq list
# this code was get from http://www.peterbe.com/plog/uniqifiers-benchmark
def ulist(seq, idfun=None):
	# order preserving 
	if idfun is None: 
		def idfun(x): return x 
	seen = {} 
	result = [] 
	for item in seq: 
		marker = idfun(item) 
		if marker in seen: continue 
		seen[marker] = 1 
		result.append(item) 
	return result

class SearchForm(forms.Form):
	user = forms.ChoiceField(label='Клиент', choices=[(u.userid.id, u.userid.login) for u in ServiceTelUparams.objects.all()])
	date_start = forms.DateField(label='Начальная дата', required=False, widget=extras.SelectDateWidget(years=range(2000, 2010, 1)))
	date_end = forms.DateField(label='Конечная дата', required=False, widget=extras.SelectDateWidget(years=range(2010, 2000, -1)))
	src_number = forms.CharField(label='SRC номер', max_length=20, required=False)
	dst_number = forms.CharField(label='DST номер', max_length=20, required=False)
	src_trunk = forms.CharField(label='SRC поток', max_length=20, required=False)
	dst_trunk = forms.CharField(label='DST поток', max_length=20, required=False)
	in_call = forms.ChoiceField(label='Направление', choices=IN_CALL) 
	group_by = forms.ChoiceField(label='Группировать по', choices=GROUP_BY)

def show_search(request):
	debug = ''
	
	sts = ServiceTelStats.objects.all()
	if request.method == 'POST':
		search_form = SearchForm(request.POST)
		if search_form.is_valid():
			if (request.POST['src_number'] != ''):
				sts = ServiceTelStats.objects.filter(src_number=request.POST['src_number'])
			if (request.POST['dst_number'] != ''):
				sts = sts.filter(dst_number=request.POST['dst_number'])
			if (request.POST['src_trunk'] != ''):
				sts = sts.filter(src_trunk=request.POST['src_trunk'])
			if (request.POST['dst_trunk'] != ''):
				sts = sts.filter(dst_trunk=request.POST['dst_trunk'])
			if (request.POST['in_call'] != '-1'):
				sts = sts.filter(in_call=request.POST['in_call'])

			sts = sts.filter(storetime__gte=datetime.date(int(request.POST['date_start_year']), int(request.POST['date_start_month']), int(request.POST['date_start_day'])) )

			sts = sts.filter(storetime__lte=datetime.date(int(request.POST['date_end_year']), int(request.POST['date_end_month']), int(request.POST['date_end_day'])) )

			sts = sts.filter(userid=int(request.POST['user']));

			#	GROUPING STATS
			sts_tmp = sts
			sts_done = []
			group_by = request.POST['group_by']
			if (group_by == 'users'):
				userids = [s.userid.id for s in ServiceTelStats.objects.all()]
				userids = ulist(userids)
#				debug = userids
				for uid in userids:
					user = Users.objects.get(pk=uid)
					stats = sts.filter(userid=user)
#					debug = stats
					user_cost = 0
					user_duration = 0
					for s in stats:
						user_cost =0
#						user_duration += s.duration
#					s.cost = user_cost
#					s.duration = user_duration
#					sts_done.append(s)
#				sts = sts_done
						
					
#				sts = ServiceTelStats.objects.all()

	else:
		search_form = SearchForm()

#	debug = connection.queries
	all_cost = 0
	all_duration = 0

	fields  = [f.name for f in ServiceTelStats._meta.fields]

	rezults_table = '<tr>'

	for verbose_name in [f.verbose_name for f in ServiceTelStats._meta.fields]:
		tmp = '<th>%s</th>' % verbose_name
		rezults_table += tmp.decode('utf-8')
	rezults_table = rezults_table + '</tr>'

	for o in sts:
		rezults_table += '<tr>'
		for i in fields:
			tmp = '<td>%s</td>' % getattr(o, i)
			rezults_table += tmp.decode('utf-8')
		rezults_table = rezults_table + '</tr>'
		all_cost += o.cost
		all_duration += o.duration

	
	return render_to_response('./tel/aggregate_stat.html',
		{
		'search_form': search_form,
		'rezults_table': mark_safe(rezults_table),
		'all_cost': all_cost,
		'all_duration': all_duration,
		'debug': debug,
		'WITHOUT_COST': settings.WITHOUT_COST
		},
		context_instance=RequestContext(request))


