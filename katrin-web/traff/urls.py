from django.conf.urls.defaults import *

urlpatterns = patterns('katrin-web.traff.views',
	(r'^$', 'show_users'),
    (r'^detail/(\d+)/$', 'show_user_detail_month', {'year': 0, 'month':0}),
    (r'^detail/(\d+)/(\d+)/(\d+)/$', 'show_user_detail_month'),
    (r'^detail/(\d+)/(\d+)/(\d+)/(\d+)/$', 'show_user_detail_day'),
    (r'^bymonth/$', 'stats_users_by_month', {'year': 0, 'month':0}),
    (r'^bymonth/(\d+)/(\d+)/$', 'stats_users_by_month'),

)
