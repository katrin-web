# vim: set fileencoding=utf-8 :

from django.db import models
from katrin.models import *

class ServiceTraffFilters(models.Model):
	id = models.IntegerField(primary_key=True)
	title = models.CharField(max_length=40)
	tariffid = models.ForeignKey(Tariffs,db_column='tariffid')
	priority = models.IntegerField()
	store_stat = models.IntegerField()
	network = models.CharField(max_length=60)
	netmask = models.CharField(max_length=60)
	port = models.IntegerField()
	proto = models.IntegerField()
	permeginput = models.DecimalField(max_digits=5, decimal_places=2)
	permegoutput = models.DecimalField(max_digits=5, decimal_places=2)
	speedin = models.CharField(max_length=30, null=True, blank=True)
	speedin_max = models.CharField(max_length=30, null=True, blank=True)
	speedout = models.CharField(max_length=30, null=True, blank=True)
	speedout_max = models.CharField(max_length=30, null=True, blank=True)
	day_hours = models.CharField(max_length=150, null=True, blank=True)
	week_days = models.CharField(max_length=60, null=True, blank=True)
	month_days = models.CharField(max_length=180, null=True, blank=True)
	year_months = models.CharField(max_length=120, null=True, blank=True)
	class Meta:
		db_table = u'service_traff_filters'
		verbose_name = 'фильтр трафика'
		verbose_name_plural = 'Фильтры трафика'
	def __unicode__(self):
			return "%s" % (self.title)
	class Admin:
		fields = (
			('Основные параметры', {'fields': ('title', 'tariffid', 'priority', 'store_stat')}),
			('Специфические параметры', {'fields': ('network', 'netmask', 'port', 'proto', 'permeginput', 'permegoutput', 'speedin', 'speedin_max', 'speedout', 'speedout_max', 'day_hours', 'week_days', 'month_days', 'year_months')}),
		)
		list_display = ("title", "tariffid", "priority", "store_stat", "network", "netmask", 'port', 'proto', 'permeginput', 'permegoutput')
		ordering = ["tariffid"]
		search_fields = ("tariffid")
		list_filter = ("tariffid",)


class ServiceTraffStats(models.Model):
	userid = models.IntegerField()
	storetime = models.DateTimeField()
	cost = models.DecimalField(max_digits=5, decimal_places=2)
	addr = models.IntegerField(null=True, blank=True)
	src_port = models.IntegerField(null=True, blank=True)
	dst_port = models.IntegerField(null=True, blank=True)
	bytes = models.IntegerField()
	proto = models.IntegerField(null=True, blank=True)
	in_traff = models.IntegerField()
	class Meta:
		db_table = u'service_traff_stats'
		verbose_name_plural = 'Статистика по трафику'
	class Admin:
			pass

class ServiceTraffStatsCache(models.Model):
	storetime = models.DateTimeField()
	bytes = models.IntegerField()
	userid = models.IntegerField()
	filterid = models.IntegerField()
	in_traff = models.IntegerField()
	cost = models.DecimalField(max_digits=5, decimal_places=2)
	class Meta:
		db_table = u'service_traff_stats_cache'

class ServiceTraffUparams(models.Model):
	id = models.IntegerField(primary_key=True)
	userid = models.ForeignKey(Users,db_column='userid')
	access_type = models.CharField(max_length=45)
	ip = models.CharField(max_length=60)
	class Meta:
		db_table = u'service_traff_uparams'
		verbose_name = 'параметры трафика'
		verbose_name_plural = 'Параметры трафика'
	class Admin:
		list_display = ("userid", "access_type", "ip")
		ordering = ["userid"]
		search_fields = ("ip", "userid")

