# vim: set fileencoding=utf-8 :
# TODO: Replace all aggregate query to DB API when it'll be work in Django.
# See: http://code.djangoproject.com/ticket/3566
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.conf import settings
from katrin.models import Users
from traff.models import ServiceTraffFilters
import datetime, time
import socket, struct
import operator
from calendar import *
from dateutil.relativedelta import relativedelta
from django.db import connection

class days_of_month:
    def __init__(self, day, download, upload, cost):
	self.day = day
	self.download = download
	self.upload = upload
	self.cost = cost

class Traffic:
    def __init__(self, download, upload, cost_download, cost_upload):
		self.download = download
		self.upload = upload
		self.cost_download = cost_download
		self.cost_upload = cost_upload

class UTraffic(Traffic):
    def __init__(self, userid, login, download, upload, cost_download, cost_upload):
		Traffic.__init__(self, download, upload, cost_download, cost_upload)
		self.userid = userid
		self.login = login

class traff_by_ports_and_protocols:
    def __init__(self, direction, proto, src_port, dst_port, bytes, cost):
	    self.direction = direction
	    self.proto = proto
	    self.src_port = src_port
	    self.dst_port = dst_port
	    self.bytes = bytes
	    self.cost = cost

class traff_by_ip(Traffic):
    def __init__(self, ip, download, upload, cost_download, cost_upload):
		Traffic.__init__(self, download, upload, cost_download, cost_upload)
		self.ip = ip

class traff_by_filter(Traffic):
    def __init__(self, filter, download, upload, cost_download, cost_upload):
		Traffic.__init__(self, download, upload, cost_download, cost_upload)
		self.filter = filter

def show_users(request):
        object_list = Users.objects.order_by('id')
        return render_to_response('traff_list_users.html',
                        {'users': object_list,
						'year': datetime.date.today().year,
						'month': datetime.date.today().month,
						'WITHOUT_COST': settings.WITHOUT_COST
						},
			context_instance=RequestContext(request))
def show_user_detail_month(request,userid,year,month):
	year = int(year)
	month = int(month)



	date_today = datetime.date.today()
	if (year==0):
		year = date_today.year
	if (month==0):
		month = date_today.month
	user = Users.objects.get(id=userid)
	month_download = 0
	month_upload = 0
	date_today = datetime.date.today();
	date_start = datetime.datetime(year, month, 1, 0, 0, 0)
	date_end = date_start + relativedelta(months=+1)

	cursor = connection.cursor()

#		if (user.ip == socket.inet_ntoa(struct.pack('!L', stat.dst_addr))):
	if (month==12):
		end_month=1
		end_year=year+1
	else:
		end_month=int(month)+1
		end_year=year

	month_download = 0
	month_upload = 0

	dom=[]
	for day in range(1,monthrange(date_today.year,date_today.month)[1]+1):
		end_year=year
		end_month=month
		end_day=day+1
		if (day==monthrange(date_today.year,date_today.month)[1]):
			end_day=1
			if (month==12):
				end_month=1
				end_year=year+1
			else:
				end_month=month+1

		if (year==date_today.year and month==date_today.month and day==date_today.day):
			cursor.execute("SELECT sum(bytes*in_traff), sum(cost*in_traff), sum(bytes*(not in_traff)), sum(cost*(not in_traff)) FROM service_traff_stats WHERE storetime BETWEEN '%s-%s-%s 00:00:00' AND '%s-%s-%s 00:00:00' AND userid=%s", (year, month, day, end_year, end_month, end_day, userid))
		else:
			cursor.execute("SELECT sum(bytes*in_traff), sum(cost*in_traff), sum(bytes*(not in_traff)), sum(cost*(not in_traff)) FROM service_traff_stats_cache WHERE storetime='%s-%s-%s 00:00:00' AND userid=%s", (year, month, day, userid))
		row = cursor.fetchone()

		day_download = bytes2Xbytes(0)
		day_upload = bytes2Xbytes(0)
		day_download_cost = 0
		day_upload_cost = 0
		if row!=None:
			if row[0]!=None:
				month_download = month_download + row[0]
				day_download = bytes2Xbytes(row[0])
			if row[2]!=None:
				month_upload = month_upload + row[2]
				day_upload = bytes2Xbytes(row[2])
			if row[1]!=None:
				day_download_cost = row[1]
			if row[3]!=None:
				day_upload_cost = row[3]

		
		day_cost = day_download_cost + day_upload_cost
		day_cost = "%1.2f" % (day_cost)
		dom.append(days_of_month(day, day_download, day_upload, day_cost))

	month_upload = bytes2Xbytes(month_upload)
	month_download = bytes2Xbytes(month_download)

	#traff by filter
	cursor.execute("SELECT filterid,sum(bytes*in_traff),sum(bytes*(not in_traff)), sum(cost*in_traff),sum(cost*(not in_traff)) FROM service_traff_stats WHERE userid=%s AND storetime BETWEEN %s AND %s GROUP BY filterid ORDER BY sum(bytes) DESC", (userid, date_start, date_end));
	rows = cursor.fetchall()
	tbfilter=get_traff_by_filter(rows)


	return render_to_response('traff_user_detail_month.html',
			{'month_download': month_download,
				'month_upload': month_upload,
				'month': month,
				'user': user,
				'year': year,
				'days_of_month': dom,
				'traff_by_filter': tbfilter,
				'WITHOUT_COST': settings.WITHOUT_COST
				},
			context_instance=RequestContext(request))

def show_user_detail_day(request,userid,year,month,day):
	# amount first ip with max bytes
	amount_ip = 25

	year = int(year)
	month = int(month)
	day = int(day)

	date_start = datetime.datetime(year, month, day, 0, 0, 0)
	difference1 = datetime.timedelta(days=1)
	date_end = date_start + difference1

	cursor = connection.cursor()

	#traff by ip
	cursor.execute("SELECT INET_NTOA(addr),sum(bytes*in_traff),sum(bytes*(not in_traff)), sum(cost*in_traff),sum(cost*(not in_traff)) FROM service_traff_stats WHERE userid=%s AND storetime BETWEEN %s AND %s GROUP BY addr ORDER BY sum(bytes) DESC LIMIT %s", (userid, date_start, date_end, amount_ip));
	rows = cursor.fetchall()
	tbip=[]
	for row in rows:
		bytes_download = bytes2Xbytes(row[1])
		bytes_upload = bytes2Xbytes(row[2])
		cost_download = "%1.2f" % (float(row[3]))
		cost_upload = "%1.2f" % (float(row[4]))
		tbip.append(traff_by_ip(row[0], bytes_download, bytes_upload, cost_download, cost_upload))

	#traff by filter
	cursor.execute("SELECT filterid,sum(bytes*in_traff),sum(bytes*(not in_traff)), sum(cost*in_traff),sum(cost*(not in_traff)) FROM service_traff_stats WHERE userid=%s AND storetime BETWEEN %s AND %s GROUP BY filterid ORDER BY sum(bytes) DESC", (userid, date_start, date_end))

	rows = cursor.fetchall()
	tbfilter = get_traff_by_filter(rows)

	cursor.execute("SELECT in_traff,proto,src_port,dst_port,sum(bytes),sum(cost) FROM service_traff_stats WHERE userid=%s AND storetime BETWEEN %s AND %s GROUP BY in_traff, proto, src_port ORDER BY sum(bytes) DESC", (userid, date_start, date_end));
	rows = cursor.fetchall()
	pp=[]
	for row in rows:
		bytes = bytes2Xbytes(row[4])
		cost = "%1.2f" % (float(row[5]))
		pp.append(traff_by_ports_and_protocols(row[0], row[1], row[2], row[3], bytes, cost));

	return render_to_response('traff_user_detail_day.html',
			{
			'amount_ip': amount_ip,
			'traff_by_ip': tbip,
			'traff_by_filter': tbfilter,
			'traff_by_ports_and_protocols': pp,
			'year': year,
			'month': month,
			'day': day,
			'WITHOUT_COST': settings.WITHOUT_COST
			},
			context_instance=RequestContext(request))

def stats_users_by_month(request,year,month):
	date_today = datetime.date.today()
	if (year==0):
		year = request.POST.get('year', str(date_today.year))
	if (month==0):
		month = request.POST.get('month', str(date_today.month))
	year = int(year)
	month = int(month)

	date_start = datetime.datetime(year, month, 1, 0, 0, 0)
	date_end = date_start + relativedelta(months=+1)


	traff=[]
	from django.db import connection
	cursor = connection.cursor()
#	end_day = monthrange(end_year,end_month)[1]
	users = Users.objects.order_by('id')

	all_download = 0
	all_upload = 0
	all_cost_download = 0
	all_cost_upload = 0

	for user in users:
		#get download, upload and cost for it
		cursor.execute("SELECT sum(bytes*in_traff), sum(bytes*(not in_traff)), sum(cost*in_traff), sum(cost*(not in_traff)) FROM service_traff_stats_cache WHERE storetime BETWEEN %s AND %s AND userid=%s", (date_start, date_end, user.id))
		row = cursor.fetchone()
		if row[0] is None:
			download = 0
		else:
			download = row[0]
		all_download = all_download + download
		download = bytes2Xbytes(download)

		if row[1] is None:
			upload = 0
		else:
			upload = row[1]
		all_upload = all_upload + upload
		upload = bytes2Xbytes(upload)

		if row[2] is None:
			cost_download = 0
		else:
			cost_download = row[2]
		all_cost_download = all_cost_download + cost_download
	
		if row[3] is None:
			cost_upload = 0
		else:
			cost_upload = row[3]
		all_cost_upload = all_cost_upload + cost_upload

		traff.append(UTraffic(user.id, user.login, download, upload, cost_download, cost_upload))

	traff.sort(key=operator.attrgetter('cost_download'),reverse=True)
	all_download = bytes2Xbytes(all_download)
	all_upload = bytes2Xbytes(all_upload)
	all_cost_download = "%1.2f" % (all_cost_download)
	all_cost_upload = "%1.2f" % (all_cost_upload)

	for tr in traff:
		tr.cost_download = "%1.2f" % (tr.cost_download)
		tr.cost_upload = "%1.2f" % (tr.cost_upload)

	#traff by filter
	cursor.execute("SELECT filterid,sum(bytes*in_traff),sum(bytes*(not in_traff)), sum(cost*in_traff),sum(cost*(not in_traff)) FROM service_traff_stats_cache WHERE storetime BETWEEN %s AND %s GROUP BY filterid ORDER BY sum(bytes) DESC", (date_start, date_end))
	rows = cursor.fetchall()
	tbfilter=get_traff_by_filter(rows)



	return render_to_response('traff_stats_users_by_month.html',
			{'traff': traff,
			'month': month,
			'months': range(1,13),
			'year': year,
			'years': range(date_today.year-3,date_today.year+1),
			'all_download': all_download,
			'all_upload': all_upload,
			'all_cost_download': all_cost_download,
			'all_cost_upload': all_cost_upload,
			'traff_by_filter': tbfilter,
			'WITHOUT_COST': settings.WITHOUT_COST
			},
			context_instance=RequestContext(request)
			)

def bytes2Xbytes(bytes):
	if (bytes >= 1073741824):
		rez = "%1.3f Gb" % (bytes/1073741824)
	else:
		if (bytes >= 1048576):
			rez = "%1.2f Mb" % (bytes/1048576)
		else:
			if (bytes >= 1024):
				rez = "%1.1f Kb" % (bytes/1024)
			else:
				rez = "%s bytes" % (bytes)
	return rez

def get_traff_by_filter(rows):
	tbfilter = []
	for row in rows:
		filter_title = ServiceTraffFilters.objects.get(pk=row[0]).title;
		bytes_download = bytes2Xbytes(row[1])
		bytes_upload = bytes2Xbytes(row[2])
		cost_download = "%1.2f" % (float(row[3]))
		cost_upload = "%1.2f" % (float(row[4]))
		tbfilter.append(traff_by_filter(filter_title, bytes_download, bytes_upload, cost_download, cost_upload))
	return tbfilter


